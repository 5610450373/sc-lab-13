package Q2;

public class ProducerConsumerMain {
	public static void main(String[] args){
		Q q = new Q();
		Consumer con = new Consumer(q);
		Producer pro = new Producer(q);
		
		Thread c = new Thread(con);
		Thread p = new Thread(pro);
		
		c.start();
		p.start();
	}

}

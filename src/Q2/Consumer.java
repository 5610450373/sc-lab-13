package Q2;

import java.util.Date;

public class Consumer implements Runnable {
	Q q;
	
	public Consumer (Q q){
		this.q = q;
	}
	public void run(){
		for(int i = 0;i<100;i++){
			try {
				dequeue();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void dequeue(){
		String word = q.dequeue();
		System.out.println(word);
	}

}

package Q2;

import java.util.Date;
import java.util.Queue;

public class Producer implements Runnable {
	Q q;
	
	public Producer (Q q){
		this.q = q;
	}
	public void run(){
		for(int i = 0;i<100;i++){
			try {
				enqueue();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void enqueue(){
		q.enqueue(new Date().toString());
	}

}

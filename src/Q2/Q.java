package Q2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Q {
	private Queue<String> q = new LinkedList<String>();
	private Lock EmptyQueue;
	private Condition con;
	private int count;
	public String date;
	public Q(){
		count = 0;
		EmptyQueue = new ReentrantLock();
		con = EmptyQueue.newCondition();
	}
	
	public String dequeue(){
		EmptyQueue.lock();
		try{
			while(q.peek()==null){
				con.await();	}
		date = q.poll();
		con.signalAll();
		return date;}
		catch(InterruptedException e){System.err.println("Interrupted Error");}
		finally{
		EmptyQueue.unlock();
		return date;}
	}
	public void enqueue(String word){
		EmptyQueue.lock();
		try{while(q.size()>=10){con.await();}
		q.add(word);
		con.signalAll();}
		catch(InterruptedException e){System.err.println("Interrupted Error");}
		finally{EmptyQueue.unlock();}
	}
	public Queue<String> getQueue(){
		return q;
	}

}
